# KWin Scripts

These are my KWin scripts. They should work under KWin with KDE Plasma 5 or KDE Plasma 6.

`centerwindow` makes a key-binding available to center the active window in the center of the screen.

`move-windows-to-desktops` is to move windows to another desktop AND change desktop at the same time, instead of the default behavior in KDE (at the time of writing this), where the window is only moved to another screen but the desktop stays the same. It provides key-bindings for up to 20 desktops.

It is recommended to put the desired script folder(s) into `~/.local/share/kwin/scripts/`. You can run the following commands from wherever you save this repo to install each respective script.



`centerwindow`:

KDE Plasma 5:

```shell
kpackagetool5 --type=KWin/Script -i ./centerwindow/
```

KDE Plasma 6:

```shell
kpackagetool6 --type=KWin/Script -i ./centerwindow/
```

`move-windows-to-desktops`

KDE Plasma 5:

```shell
kpackagetool5 --type=KWin/Script -i ./move-windows-to-desktops/
```

KDE Plasma 6:

```shell
kpackagetool6 --type=KWin/Script -i ./move-windows-to-desktops/
```